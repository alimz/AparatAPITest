//
//  ViewController.swift
//  AparatAPI
//
//  Created by Ali`s Macbook Pro on 7/25/17.
//  Copyright © 2017 Ali`s Macbook Pro. All rights reserved.
//

import UIKit
import Unbox

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    //MARK: - Properties
    var collectionView: UICollectionView!
    var offset = 0
    let perpage = 10
    var isLoadingMoreData = false
    var result : RootClass?
    var isGridType = true
    
    //MARK: - Actions
    
    func changeLayout(sender:UIButton) {
        isGridType = !isGridType
        self.collectionView.reloadData()
        self.collectionView.performBatchUpdates({ 
            UIView.animate(withDuration: 0.2) { () -> Void in
                self.collectionView.collectionViewLayout.invalidateLayout()
                self.collectionView.setCollectionViewLayout(self.isGridType ? SquareColectionViewFlowLayout() : RectangleCollectionViewFlowLayout(), animated: true)
            }
            
            self.configNavBarButton()
        }) { (finished) in
            
        }
        
    }
    
    //MARK: - Functions
    override func viewDidLoad() {
        super.viewDidLoad()
       
        configView()
        getLastVideos(perpage: perpage,pageBack: nil,pageForward: nil)
    }
    
    func configView() {
        let layout: UICollectionViewFlowLayout = SquareColectionViewFlowLayout()
        collectionView = UICollectionView(frame: CGRect(x: 8, y: 8, width: self.view.frame.size.width - 16, height: self.view.frame.size.height - 16), collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ListCollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = UIColor.lightGray
        collectionView.showsVerticalScrollIndicator = false
        self.view.addSubview(collectionView)
        self.view.backgroundColor = UIColor.lightGray
        self.title = "آخرین ویدیو ها"
        configNavBarButton()
    }
    
    
    func configNavBarButton() {
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: isGridType ? "list type" : "grid type"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(changeLayout(sender:)), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setLeftBarButtonItems([item1], animated: true)
    }
    
    func getLastVideos(perpage:Int,pageBack:String?,pageForward:String?) {
        
        if isLoadingMoreData {
            return
        }
        
        let tryingToPage = pageForward != nil || pageBack != nil
        
        isLoadingMoreData = true
        ServerRequest.getLastVideos(perpage: perpage, pageBack: pageBack,pageForward:pageForward ) { (data, error) in
            do {
                let result : RootClass = try unbox(data: data!)
                if tryingToPage {
                    for video in result.lastvideos! {
                        self.result?.lastvideos!.append(video)
                    }
                    self.result?.ui = result.ui
                }else {
                    self.result = result
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                self.isLoadingMoreData = false
            }catch let error{
                print(error)
                self.isLoadingMoreData = false
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Collcetion View
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return result != nil ? result!.lastvideos!.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ListCollectionViewCell

        //cell apperance
        cell.backgroundColor = UIColor.white
        cell.isGridType = isGridType
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: -2, height: 2)
        
        //cell data
        cell.imageView.kf.indicatorType = .activity
        cell.imageView.kf.setImage(with: URL(string: (result?.lastvideos?[indexPath.item].smallPoster)!))
       
        cell.titleLabel.text = result?.lastvideos?[indexPath.item].title!
       
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let shouldBePaged = result?.ui?.pagingForward != nil
        if indexPath.item + 1 == (result?.lastvideos?.count)! && shouldBePaged {
            self.getLastVideos(perpage: perpage, pageBack: nil, pageForward: result?.ui?.pagingForward)
        }
    }
    
    
    
}

