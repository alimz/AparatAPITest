//
//  RectangleCollectionViewFlowLayout.swift
//  AparatAPI
//
//  Created by Ali`s Macbook Pro on 7/25/17.
//  Copyright © 2017 Ali`s Macbook Pro. All rights reserved.
//

import UIKit

class RectangleCollectionViewFlowLayout: UICollectionViewFlowLayout {
    var numberOfItemsPerPage: Int = 4 {
        didSet {
            invalidateLayout()
        }
    }
    
    override func prepare() {
        super.prepare()
        
        if let collectionView = self.collectionView {
            var newItemSize = itemSize
            
            // Always use an item count of at least 1
            let itemsPerPage = CGFloat(max(numberOfItemsPerPage, 1))
            
            // Calculate the sum of the spacing between cells
            let totalSpacing = minimumInteritemSpacing * (itemsPerPage - 1.0)
            
            // Calculate how height items should be
            newItemSize.height = (collectionView.frame.size.height - totalSpacing) / itemsPerPage
            
            // Use the aspect ratio of the current item size to determine how long the items should be
           // if itemSize.height > 0 {
               // let itemAspectRatio = (itemSize.width / itemSize.height)
                newItemSize.width = collectionView.frame.size.width
           // }
            
            // Set the new item size
            itemSize = newItemSize
            minimumLineSpacing = 8
            minimumInteritemSpacing = 8
            
        }
    }
    
}
