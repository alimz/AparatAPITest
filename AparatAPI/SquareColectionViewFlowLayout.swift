//
//  SquareColectionViewFlowLayout.swift
//  AparatAPI
//
//  Created by Ali`s Macbook Pro on 7/25/17.
//  Copyright © 2017 Ali`s Macbook Pro. All rights reserved.
//

import UIKit


class SquareColectionViewFlowLayout: UICollectionViewFlowLayout {
    
    var numberOfItemsPerRow: Int = 1 {
        didSet {
            invalidateLayout()
        }
    }
    
    override func prepare() {
        super.prepare()
        
        if let collectionView = self.collectionView {
            var newItemSize = itemSize
            
            // Always use an item count of at least 1
            let itemsPerRow = CGFloat(max(numberOfItemsPerRow, 1))
            
            // Calculate the sum of the spacing between cells
            let totalSpacing = minimumInteritemSpacing * (itemsPerRow - 1.0)
            
            // Calculate how wide items should be
            newItemSize.width = (collectionView.frame.size.width - totalSpacing) / itemsPerRow
            
            // Use the aspect ratio of the current item size to determine how tall the items should be
            if itemSize.height > 0 {
                let itemAspectRatio = itemSize.width / itemSize.height
                newItemSize.height = newItemSize.width / itemAspectRatio
            }
            
            // Set the new item size
            itemSize = newItemSize
            minimumLineSpacing = 8
            minimumInteritemSpacing = 8
            
        }
    }
    
}
