//
//  ServerRequest.swift
//  AparatAPI
//
//  Created by Ali`s Macbook Pro on 7/25/17.
//  Copyright © 2017 Ali`s Macbook Pro. All rights reserved.
//

import Foundation


class ServerRequest {
    
    
    //MARK: - Properties
    
    static func sendRequest(method:String,urlStructure:String,params:[String:AnyObject]?,completionHandler:@escaping (_ data:Data?,_ error:Error?) ->()) {
        let req = NSMutableURLRequest(url: URL(string:urlStructure)!)
        req.httpMethod = method
        URLSession.shared.dataTask(with: req as URLRequest) { data, response, error in
            if error != nil {
                //Your HTTP request failed.
                print(error?.localizedDescription as Any)
            } else {
                //Your HTTP request succeeded
                completionHandler(data, error)
            }
            }.resume()
        
    }
    
    
    //MARK: - Functions
    static func getLastVideos(perpage:Int,pageBack:String?,pageForward:String?,completionHandler: @escaping ( _ data :Data?,_ error :Error?) ->()) {
        
        if pageBack == nil && pageForward == nil {
            sendRequest(method: "GET", urlStructure: "http://aparat.com/etc/api/lastVideos/perpage/\(perpage)", params: nil, completionHandler: completionHandler)
        }else {
            sendRequest(method: "GET", urlStructure: "\(pageBack == nil ? pageForward! : pageBack!)", params: nil, completionHandler: completionHandler)
        }
        
    }
    
}

