//
//  SquareCollectionViewCell.swift
//  AparatAPI
//
//  Created by Ali`s Macbook Pro on 7/25/17.
//  Copyright © 2017 Ali`s Macbook Pro. All rights reserved.
//

import UIKit
import Kingfisher

class ListCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Properties
    var imageView : UIImageView!
    var titleLabel : UILabel!
    var isGridType = true
    
    //MARK: - Functions
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Initialize Subviews
        imageView = isGridType ? UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 70)) : UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.height, height: self.frame.height))
        titleLabel = isGridType ? UILabel(frame: CGRect(x: 8, y: imageView.frame.size.height , width: self.frame.width - 16, height: 70)) : UILabel(frame: CGRect(x: self.frame.height + 8, y: 0 , width: self.frame.width - self.frame.height - 16, height: self.frame.height))
        self.addSubview(imageView)
        self.addSubview(titleLabel)
        self.clipsToBounds = true
        imageView.backgroundColor = UIColor(hue: 6.0, saturation: 0.78, brightness: 0.75, alpha: 1)
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        titleLabel.textColor = UIColor.darkGray
        titleLabel.textAlignment = .right
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont(name: "IRANSansMobile", size: 16)
        titleLabel.text = "متن"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        
    }
    
    override func layoutSubviews() {
        imageView.frame  = isGridType ? CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 70) : CGRect(x: 0, y: 0, width: self.frame.height, height: self.frame.height)
        titleLabel.frame = isGridType ? CGRect(x: 8, y: imageView.frame.size.height , width: self.frame.width - 16, height: 70) : CGRect(x: self.frame.height + 8, y: 0 , width: self.frame.width - self.frame.height - 16, height: self.frame.height)
        layoutIfNeeded()
        
    }
    
    
}
