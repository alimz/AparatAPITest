//
//	Ui.swift
//
//	Create by Ali Moazenzadeh on 25/7/2017
//	Copyright © 2017. All rights reserved.


import Foundation
import Unbox

struct Ui: Unboxable {

	let pagingBack : String?
	let pagingForward : String?


	init(unboxer: Unboxer){
		pagingBack = unboxer.unbox(key:"pagingBack")
		pagingForward = unboxer.unbox(key:"pagingForward")
	}

}
