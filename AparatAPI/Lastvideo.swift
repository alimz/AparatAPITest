//
//	Lastvideo.swift
//
//	Create by Ali Moazenzadeh on 25/7/2017
//	Copyright © 2017. All rights reserved.


import Foundation
import Unbox

struct Lastvideo: Unboxable {

	let sphere : Bool?
	let autoplay : Bool?
	let bigPoster : String?
	let deleteurl : String?
	let duration : String?
	let frame : String?
	let id : String?
	let isHidden : Bool?
	let official : String?
	let process : String?
	let profilePhoto : String?
	let sdate : String?
	let sdateTimediff : Int?
	let senderName : String?
	let smallPoster : String?
	let title : String?
	let uid : String?
	let userid : String?
	let username : String?
	let videoDateStatus : String?
	let visitCnt : Int?


	init(unboxer: Unboxer){
		sphere = unboxer.unbox(key:"360d")
		autoplay = unboxer.unbox(key:"autoplay")
		bigPoster = unboxer.unbox(key:"big_poster")
		deleteurl = unboxer.unbox(key:"deleteurl")
		duration = unboxer.unbox(key:"duration")
		frame = unboxer.unbox(key:"frame")
		id = unboxer.unbox(key:"id")
		isHidden = unboxer.unbox(key:"isHidden")
		official = unboxer.unbox(key:"official")
		process = unboxer.unbox(key:"process")
		profilePhoto = unboxer.unbox(key:"profilePhoto")
		sdate = unboxer.unbox(key:"sdate")
		sdateTimediff = unboxer.unbox(key:"sdate_timediff")
		senderName = unboxer.unbox(key:"sender_name")
		smallPoster = unboxer.unbox(key:"small_poster")
		title = unboxer.unbox(key:"title")
		uid = unboxer.unbox(key:"uid")
		userid = unboxer.unbox(key:"userid")
		username = unboxer.unbox(key:"username")
		videoDateStatus = unboxer.unbox(key:"video_date_status")
		visitCnt = unboxer.unbox(key:"visit_cnt")
	}

}
