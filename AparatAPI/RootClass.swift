//
//	RootClass.swift
//
//	Create by Ali Moazenzadeh on 25/7/2017
//	Copyright © 2017. All rights reserved.


import Foundation
import Unbox

struct RootClass: Unboxable {

	var lastvideos : [Lastvideo]?
	var ui : Ui?


	init(unboxer: Unboxer){
		lastvideos = unboxer.unbox(key:"lastvideos")
		ui = unboxer.unbox(key:"ui")
	}

}
