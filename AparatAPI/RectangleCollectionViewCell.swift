//
//  RectangleCollectionViewCell.swift
//  AparatAPI
//
//  Created by Ali`s Macbook Pro on 7/25/17.
//  Copyright © 2017 Ali`s Macbook Pro. All rights reserved.
//

import UIKit
import Kingfisher

class RectangleCollectionViewCell: UICollectionViewCell {
    //MARK: - Properties
    var imageView : UIImageView!
    var titleLabel : UILabel!
    
    
    //MARK: - Functions
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Initialize Subviews
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.height, height: self.frame.height))
        titleLabel = UILabel(frame: CGRect(x: self.frame.height + 8, y: 0 , width: self.frame.width - self.frame.height - 16, height: self.frame.height))
        self.addSubview(imageView)
        self.addSubview(titleLabel)
        self.clipsToBounds = true
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        titleLabel.textColor = UIColor.darkGray
        titleLabel.textAlignment = .right
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont(name: "IRANSansMobile", size: 16)
        titleLabel.text = "متن"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        
    }
}
