//
//  AppDelegate.swift
//  AparatAPI
//
//  Created by Ali`s Macbook Pro on 7/25/17.
//  Copyright © 2017 Ali`s Macbook Pro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var NavigationController: UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        NavigationController = UINavigationController()
        let homeViewController = ViewController()
        NavigationController?.viewControllers.append(homeViewController) 
        window!.rootViewController = NavigationController
        window!.makeKeyAndVisible()
        
        //NavigationBar Apperance
        self.NavigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "IRANSansMobile", size: 16)!]
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
       
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
       
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }


}

